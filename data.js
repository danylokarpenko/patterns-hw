export const texts = [
  "While dinosaurs were ancestrally bipedal, many extinct groups included quadrupedal species, and some were able to shift between these stances.",
  "Dinosaurs can therefore be divided into avian dinosaurs, or birds; and non-avian dinosaurs, which are all dinosaurs other than birds.",
  "While recent discoveries have made it more difficult to present a universally agreed-upon list of dinosaurs' distinguishing features, nearly all dinosaurs discovered so far share certain modifications to the ancestral skeleton, or are clear descendants of older dinosaurs showing these modifications.",
  "This text is for testing bot reaction for 30 symbols left before finish the race! All is good, especially this game:)",
  "Text5",
  "Text6",
  "Text7",
];

export const congratulations = [
  'Hello! I\'m your commentator bot.',
  'Hey, how u doing?:D.',
  'Welcome! Mr.Commentator is always happy to see you!',
  'Oooh, I\'m so glad to see you here!!!\nWe are waiting for another racers now'
];

export const listHeaders = [
  'All are very brave:',
  'Oh, unbelievable:D. Who will be the Best?',
  'They have metal fingers:',
  'They Never give up:'
];

export const finishPhrases = [
  'He did it!',
  'Are u the sun of a Dart Weider?',
  'I think his fingers are totally smashed :D',
  'FINISHED!!!!',
  'Glory To Him!'
];

export const winnerPhrases = [
  'Winner is!',
  'As always',
  'Do u need some finger-healers, dear ',
  'FINISHED first ',
  'Glory and money To Him! '
];

export default { texts };
