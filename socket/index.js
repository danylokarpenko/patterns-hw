import { texts } from '../data';
import { User, Bot } from "../services";
import {
  MAXIMUM_USERS_FOR_ONE_ROOM,
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME, SHOW_GAME_STATUS_INTERVAL,
  SECONDS_BEFORE_SHOW_PLAYERS
} from "./config";
import {
  isAllUsersReady,
  getRandomInt,
  getRooms,
  isUserActive,
  getUserCount,
  getUsersFromRoom,
  updateUser
} from './helper';

const roomBots = new Map();
const roomsMap = new Map();
const usersMap = new Map();

const updateRooms = (io, rooms) => {
  rooms.forEach((room, roomId) => {
    const userCount = getUserCount(roomId, usersMap);
    if (userCount === 0) {
      roomsMap.delete(roomId);
      roomBots.delete(roomId);
      io.in(roomId).emit('');
      return;
    }
    if (userCount >= MAXIMUM_USERS_FOR_ONE_ROOM || roomsMap.get(roomId).timerOn) {
      const room = roomsMap.get(roomId);
      roomsMap.set(roomId, { ...room, disable: true });
    } else {
      const room = roomsMap.get(roomId);
      roomsMap.set(roomId, { ...room, disable: false });
    }
    if (userCount !== room.userCount) {
      roomsMap.set(roomId, { roomId, userCount });
    }
    if (roomsMap.has(roomId)) {
      const connectedUsers = getUsersFromRoom(roomId, usersMap);
      io.in(roomId).emit('update_room', { roomId, connectedUsers, secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME });
    }
  })
  io.emit('update_rooms', getRooms(roomsMap));
}

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    if (isUserActive(io, username, socket.id)) {
      socket.emit('user_active', username);
    } else {
      const user = new User(socket.id, username, false, 0, false);
      usersMap.set(user.id, user);
    }

    let manageIntervalId;
    socket.emit('update_rooms', getRooms(roomsMap));

    const onRoomCreate = roomId => {
      if (roomsMap.has(roomId)) {
        socket.emit('show_message', 'Room name is not unique!\nTry one more time)');
        return;
      }

      socket.emit('render_room', roomId);
      socket.join(roomId, () => {
        updateUser({ id: socket.id, roomId }, usersMap);
        const connectedUsers = getUsersFromRoom(roomId, usersMap);
        const userCount = getUserCount(roomId, usersMap);
        roomsMap.set(roomId, { roomId, userCount });
        io.in(roomId).emit('join_room_done', { roomId, connectedUsers, username });
        updateRooms(io, roomsMap);
      });
    };

    const onJoinRoom = roomId => {
      const username = socket.handshake.query.username;

      socket.join(roomId, () => {
        updateUser({ id: socket.id, roomId }, usersMap);
        socket.emit('render_room', roomId);
        const connectedUsers = getUsersFromRoom(roomId, usersMap);
        io.in(roomId).emit('join_room_done', { roomId, connectedUsers, username });
        updateRooms(io, roomsMap);
      });
    };

    const onSetUserReady = roomId => {
      const { id, ready } = usersMap.get(socket.id);
      updateUser({ id, ready: !ready }, usersMap);

      if (!roomId) return;
      const connectedUsers = getUsersFromRoom(roomId, usersMap);
      io.in(roomId).emit('update_room', { roomId, connectedUsers });

      if (connectedUsers.length > 1 && isAllUsersReady(connectedUsers)) {
        const textIndex = getRandomInt(0, texts.length);

        io.in(roomId).emit('start_game', {
          roomId,
          secondsBeforeStart: SECONDS_TIMER_BEFORE_START_GAME,
          textId: textIndex,
          secondsForGame: SECONDS_FOR_GAME
        });
        // Facade
        const bot = new Bot(roomId);
        roomBots.set(roomId, bot);

        const congratulation = bot.getCongratulation();
        io.in(roomId).emit('show_bot_message', congratulation);

        setTimeout(() => {
          const racerListMessage = bot.getRacerList(usersMap);
          io.in(roomId).emit('show_bot_message', racerListMessage);
        }, SECONDS_BEFORE_SHOW_PLAYERS * 1000);

        const room = roomsMap.get(roomId);
        roomsMap.set(roomId, { ...room, gameStarted: true, disable: true, timerOn: true });
        updateRooms(io, roomsMap);
      }
    }

    const onStartBot = roomId => {
      const bot = roomBots.get(roomId);
      manageIntervalId = setInterval(() => {
        const message = bot.getRaceStatus(usersMap);
        io.in(roomId).emit('show_bot_message', message);
      }, SHOW_GAME_STATUS_INTERVAL * 1000);
    };

    const onShowGameStatus = roomId => {
      const bot = roomBots.get(roomId);
      const message = bot.getRaceStatus(usersMap);
      io.in(roomId).emit('show_bot_message', message);
    };

    const onShowWinners = roomId => {
      const bot = roomBots.get(roomId);
      clearInterval(manageIntervalId);
      const message = bot.getWinnersListMessage(usersMap);
      io.in(roomId).emit('show_bot_message', message);
    };

    const onFinishGame = roomId => {
      const connectedUsers = getUsersFromRoom(roomId, usersMap);

      const room = roomsMap.get(roomId);
      Object.assign(room, { gameStarted: false });
      roomsMap.set(roomId, room);

      socket.emit('game_finished_done', { roomId, connectedUsers });
    };

    const onUserFinished = ({ userId, roomId }) => {
      updateUser({ id: userId, finished: true }, usersMap);
      const connectedUsers = getUsersFromRoom(roomId, usersMap);
      const allFinished = connectedUsers.every(user => user.finished);

      const bot = roomBots.get(roomId);
      const message = bot.getUserFinishedMessage(userId, usersMap);
      io.in(roomId).emit('show_bot_message', message);

      if (allFinished) {
        io.in(roomId).emit('all_users_finished', { roomId });
        return;
      }
    };

    const onResetUser = ({ userId, roomId }) => {
      updateUser({ id: userId, ready: false, finished: false, progressPercentage: 0 }, usersMap);
      const connectedUsers = getUsersFromRoom(roomId, usersMap);

      io.in(roomId).emit('update_room', { roomId, connectedUsers });
    };

    const onUserUpdate = userData => {
      updateUser(userData, usersMap);
    };

    const onSetIndicator = ({ roomId, progressPercentage }) => {
      io.in(roomId).emit('set_indicator_done', { userId: socket.id, progressPercentage })
    };

    const onDisableRoom = roomId => {
      if (!roomsMap.has(roomId)) return;
      const room = roomsMap.get(roomId);
      room.disable = true;
      roomsMap.set(roomId, room);
      updateRooms(io, roomsMap);
    }

    const onDisconnect = () => {
      usersMap.delete(socket.id);
      updateRooms(io, roomsMap);
    };

    socket.on('create_room', onRoomCreate);
    socket.on('join_room', onJoinRoom);
    socket.on('user_ready', onSetUserReady);

    socket.on('start_bot', onStartBot);
    socket.on('bot_show_game_status', onShowGameStatus);
    socket.on('bot_show_winners', onShowWinners);

    socket.on('finish_game', onFinishGame);
    socket.on('user_finished', onUserFinished);
    socket.on('reset_user', onResetUser);
    socket.on('update_user', onUserUpdate);
    socket.on('set_indicator', onSetIndicator);
    socket.on('disable_room', onDisableRoom)
    socket.on("disconnect", onDisconnect);
  });
};
