export const isAllUsersReady = (users) => users.every(user => user.ready);

export const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

export const getRooms = (roomsMap) => {
    const res = [];
    roomsMap.forEach(value => res.push(value));
    return res;
};

export const isUserActive = (io, username, id) => Object.keys(io.sockets.connected)
    .find(socketId => io.sockets.connected[socketId].handshake.query.username === username && id !== socketId);

export const getUserCount = (roomId, usersMap) => {
    let count = 0;

    usersMap.forEach(user => {
        if (user.roomId === roomId) {
            count += 1;
        }
    });

    return count;
}

export const getUsersFromRoom = (roomId, usersMap) => {
    const connectedUsers = [];

    usersMap.forEach(user => {
        if (user.roomId === roomId) {
            connectedUsers.push(user);
        }
    });

    return connectedUsers;
}

export const updateUser = (user, usersMap) => {
    const { id } = user;
    const userData = usersMap.get(id);
    Object.assign(userData, user);
    usersMap.set(id, userData);
}
