import { createElement, addClass, removeClass, getTextById, getProgressPercentage } from "./helper.mjs";
import { createRoomCard, fillRoomInfo, renderUsersOnPage } from "./components.mjs";

let onKeyDown;
let gameTimerId = null;
let timeoutId = null;

const stopTimer = () => {
  clearInterval(gameTimerId);
}
const stopTimeout = () => {
  clearTimeout(timeoutId);
}

const username = sessionStorage.getItem("username");
if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const roomsContainer = document.getElementById("rooms-container");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const arena = document.getElementById("arena");
const createRoomBtn = document.getElementById("create-room-btn");

createRoomBtn.addEventListener("click", () => {
  const roomId = window.prompt('Enter room title:');
  if (!roomId) return;
  socket.emit('create_room', roomId);
});

const updateRooms = rooms => {
  const allRooms = rooms
    .filter(({ disable }) => !disable)
    .map(room => createRoomCard(socket, room));
  roomsContainer.innerHTML = "";
  roomsContainer.append(...allRooms);
};

const joinRoomDone = ({ connectedUsers = [] }) => {
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');
  removeClass(gamePage, 'game-page');

  renderUsersOnPage({ connectedUsers, username });
}

const onUpdateRoom = ({ connectedUsers = [] }) => {
  if (connectedUsers.length === 0) {
    return;
  }
  renderUsersOnPage({ connectedUsers, username });
}

socket.on('user_active', (username) => {
  sessionStorage.removeItem("username");
  window.location.replace("/login");
  window.alert(`User with username '${username}' is active!`);
})

const onSetIndicatorDone = ({ userId, progressPercentage }) => {
  const progressIndicator = document.getElementById(`${userId}-user-indicator`);
  progressIndicator.style.width = `${progressPercentage}%`;
  if (progressPercentage === 100) {
    addClass(progressIndicator, 'full-indicator');
  }
};

const onGameEnd = ({ roomId }) => {
  const timer = document.getElementById('on-game-timer');
  timer.innerText = '';
  addClass(timer, 'display-none');

  document.removeEventListener('keydown', onKeyDown);

  const backBtn = document.getElementById(`back-btn-${roomId}`);
  const readyBtn = document.getElementById(`ready-btn-${roomId}`);
  const gameText = document.getElementById('game-text');

  gameText.innerHTML = '';
  readyBtn.innerText = 'Ready';

  addClass(gameText, 'display-none');
  removeClass(backBtn, 'display-none');
  removeClass(readyBtn, 'display-none');

  socket.emit('reset_user', { userId: socket.id, roomId });
}

const onGameStart = async ({ roomId, secondsBeforeStart, textId, secondsForGame }) => {
  socket.emit('disable_room', roomId);
  socket.emit('bot_congratulation');

  const { text } = await getTextById(textId);

  const backBtn = document.getElementById(`back-btn-${roomId}`);
  const readyBtn = document.getElementById(`ready-btn-${roomId}`);
  const typingText = document.getElementById('game-text');

  addClass(backBtn, 'display-none');
  addClass(readyBtn, 'display-none');

  const timerElement = document.getElementById('on-game-timer');

  const typedSpan = createElement({
    tagName: 'span',
    className: 'typed-text display-none'
  })
  const nextSymbolSpan = createElement({
    tagName: 'span',
    className: 'underlined-text display-none'
  })
  const emptySpan = createElement({
    tagName: 'span',
    className: 'empty-text display-none'
  })
  const timerBeforeStart = createElement({
    tagName: 'span',
    className: 'timer',
    attributes: { id: 'before-game-timer' }
  })

  typingText.append(typedSpan, nextSymbolSpan, emptySpan);
  arena.append(timerBeforeStart, typingText);

  let secondsToStartLefts = secondsBeforeStart;
  timerBeforeStart.innerText = secondsToStartLefts;
  const beforeTimerId = setInterval(() => {
    secondsToStartLefts -= 1;
    timerBeforeStart.innerText = secondsToStartLefts;
  }, 1000);

  emptySpan.innerText = text;
  const typedKeys = [];
  const emptyKeys = text.split('');

  const typeSymbol = (key) => {
    const nextIndex = typedKeys.length;
    if (key !== emptyKeys[nextIndex]) {
      return;
    }
    typedKeys.push(key);
    const typedText = typedKeys.join('');
    const nextSymbol = emptyKeys[nextIndex + 1] || '';
    const emptyText = emptyKeys.join('').slice(nextIndex + 2);

    if (emptyText.length + 1 === 30) {
      socket.emit('bot_show_game_status', roomId)
    }

    typedSpan.innerText = typedText;
    nextSymbolSpan.innerText = nextSymbol;
    emptySpan.innerText = emptyText;

    const userId = socket.id;
    const progressPercentage = getProgressPercentage(typedText.length, text.length);
    socket.emit('update_user', { id: userId, progressPercentage });
    socket.emit('set_indicator', { roomId, progressPercentage});
    if (progressPercentage === 100) {
      socket.emit('user_finished', { userId, roomId });
    }
  }

  onKeyDown = (event) => {
    const { key } = event;
    typeSymbol(key);
  }

  setTimeout(() => {
    socket.emit('start_bot', roomId);
    clearInterval(beforeTimerId);
    addClass(timerBeforeStart, 'display-none');

    removeClass(timerElement, 'display-none');
    removeClass(typingText, 'display-none');
    removeClass(typedSpan, 'display-none');
    removeClass(nextSymbolSpan, 'display-none');
    removeClass(emptySpan, 'display-none');

    let secondsToEnd = secondsForGame;
    timerElement.innerText = `${secondsToEnd} seconds left`;
    gameTimerId = setInterval(() => {
      secondsToEnd -= 1;
      timerElement.innerText = `${secondsToEnd} seconds left`;
    }, 1000);

    document.addEventListener('keydown', onKeyDown);

    timeoutId = setTimeout(() => {
      stopTimer();
      socket.emit('bot_show_winners', roomId);
      socket.emit('finish_game', roomId);
    }, secondsForGame * 1000);

  }, secondsBeforeStart * 1000);
}

const onAllUserFinished = ({ roomId }) => {
  stopTimer();
  stopTimeout();
  socket.emit('bot_show_winners', roomId);
  socket.emit('finish_game', roomId);
}

const setBotMessage = (message) => {
  const textField = document.getElementById('bot-message');
  textField.innerText = message;
}

socket.on('update_room', onUpdateRoom);
socket.on("update_rooms", updateRooms);
socket.on("join_room_done", joinRoomDone);
socket.on('show_message', message => window.alert(message));
socket.on('render_room', roomId => fillRoomInfo(socket, roomId));

socket.on('set_indicator_done', onSetIndicatorDone);
socket.on('game_finished_done', onGameEnd);
socket.on('start_game', onGameStart);
socket.on('all_users_finished', onAllUserFinished);

socket.on('show_bot_message', setBotMessage);
