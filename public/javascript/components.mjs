import { createElement } from "./helper.mjs";

export const createRoomCard = (socket, {
  roomId,
  userCount
}) => {
  const card = createElement({
    tagName: 'div',
    className: 'room-card flex-centered',
    attributes: { roomId }
  })
  const userCountEl = createElement({
    tagName: 'div',
    className: 'user-count'
  });
  const roomTitleEl = createElement({
    tagName: 'span',
    className: 'room-title',
  })
  const roomButton = createElement({
    tagName: "button",
    className: "btn",
    attributes: { id: 'join-room-btn'}
  });

  userCountEl.innerText = `${userCount} joined`
  roomTitleEl.innerText = roomId;
  roomButton.innerText = 'Join Room';

  const onJoinRoom = () => {
    socket.emit("join_room", roomId);
  };

  roomButton.addEventListener("click", onJoinRoom);

  card.append(userCountEl, roomTitleEl, roomButton);

  return card;
}

export const renderUser = (user, active = false) => {
  const { id, username, ready } = user;
  const userContainer = createElement({
    tagName: 'div',
    className: 'flex-column',
    attributes: { id: `user-${id}` }
  });
  const userInfo = createElement({
    tagName: 'div',
    className: active ? 'flex-centered user-info active-user' :'flex-centered user-info',
  })

  const gameStatus = createElement({
    tagName: 'div',
    className: ready ? 'user-status-active' : 'user-status-inactive',
    attributes: { id: `status-${id}` }
  });

  const bar = createElement({ tagName: 'div', className: 'arena___progress-bar' });
  const indicator = createElement({ tagName: 'div', className: 'arena___progress-indicator', attributes: { id: `${id}-user-indicator` } });

  userInfo.innerText = username;
  userInfo.append(gameStatus);

  bar.append(indicator);
  userContainer.append(userInfo, bar);

  return userContainer;
}

export const fillRoomInfo = (socket, roomId) => {
  const roomInfo = document.getElementById("room-info");
  const arena = document.getElementById("arena");

  const roomTitleEl = createElement({
    tagName: 'span',
    className: 'room-title',
  });

  const backButton = createElement({
    tagName: "button",
    className: "btn",
    attributes: { id: `back-btn-${roomId}` }

  });

  const readyButton = createElement({
    tagName: "button",
    className: "btn",
    attributes: { id: `ready-btn-${roomId}` }
  });

  const usersContainer = createElement({
    tagName: 'div',
    className: 'flex-column',
    attributes: { id: 'connected-users' }
  })

  readyButton.innerText = 'Ready'
  roomTitleEl.innerText = roomId;
  backButton.innerText = 'Back To Rooms';

  const onBackToRoom = () => {
    socket.emit('refresh_rooms');
    window.location.reload();
  };
  const onReady = () => {
    socket.emit('user_ready', roomId);
    readyButton.innerText = readyButton.innerText.toLowerCase() === 'ready' ? 'Not Ready' : 'Ready';    
  };

  backButton.addEventListener("click", onBackToRoom);
  readyButton.addEventListener("click", onReady);

  arena.append(readyButton);
  roomInfo.append(roomTitleEl, backButton, usersContainer);
}

export const renderUsersOnPage = ({ connectedUsers, username }) => {
  const usersContainer = document.getElementById('connected-users');
  const isActive = (username1, username2) => username1 === username2;
  const renderedUsers = connectedUsers.map(user => isActive(username, user.username) ? renderUser(user, true) : renderUser(user));

  usersContainer.innerHTML = '';
  usersContainer.append(...renderedUsers);
}