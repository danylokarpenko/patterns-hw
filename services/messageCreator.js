import { congratulations, listHeaders, finishPhrases, winnerPhrases } from '../data';
import { getRandomInt } from '../socket/helper';

class MessageCreator {
    formCongratulations() {
        return congratulations;
    }

    formRacerList(racers) {
        const index = getRandomInt(0, listHeaders.length);

        const list = racers.map(racer => `Racer: ${racer.username}`).join('\n');
        const header = listHeaders[index];

        return `${header}\n${list}`;
    }

    formRaceStatus(racers) {
        const message = racers
            .sort((racer1, racer2) => racer2.progressPercentage - racer1.progressPercentage)
            .map((racer, index) => index === 0 ? `Run forward: "${racer.username}" on old, OMG, Dell machine!!!` : `Racer#${index + 1}: "${racer.username}"`).join('\n');
        return message;
    }

    formUserFinishedMessage(racer) {
        const index = getRandomInt(0, finishPhrases.length);
        const finishPhrase = finishPhrases[index];

        return `Racer ${racer.username} finished the Race!\n${finishPhrase}`;
    }

    forWinnersMessage(racers) {
        const index = getRandomInt(0, winnerPhrases.length);
        const winnerPhrase = winnerPhrases[index];

        const message = racers
            .sort((racer1, racer2) => racer2.progressPercentage - racer1.progressPercentage)
            .slice(0, 3)
            .map((racer, index) => index === 0 ? `${winnerPhrase} $$$${racer.username}$$$` : `#${index + 1} finished ${racer.username}`)
            .join('\n');

        return `Winner list:\n${message}`;
    }
}

export default MessageCreator;