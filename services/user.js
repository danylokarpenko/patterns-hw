class User {
    constructor(id, username, ready = false, progressPercentage = 0, finished = false) {
        this.id = id;
        this.username = username;
        this.ready = ready;
        this.progressPercentage = progressPercentage;
        this.finished = finished;
    }
}

export default User;