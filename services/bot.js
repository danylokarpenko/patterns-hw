import MessageCreator from './messageCreator';
import { getRandomInt, getUsersFromRoom } from '../socket/helper';

class Bot {
    constructor(roomId) {
        this.roomId = roomId;
        // Proxy
        this.messageCreator = new MessageCreator();
        this.messageCache = {};
    }

    getCongratulation() {
        
        if (!this.messageCache['congratulations']) {
            this.messageCache['congratulations'] = this.messageCreator.formCongratulations();
        }
        
        const randomIndex = getRandomInt(0, this.messageCache['congratulations'].length);
        return this.messageCache['congratulations'][randomIndex];
    }

    getRacerList(usersMap) {
        const users = getUsersFromRoom(this.roomId, usersMap);
        const message = new MessageCreator().formRacerList(users);
        return message;
    }

    getRaceStatus(usersMap) {
        const users = getUsersFromRoom(this.roomId, usersMap);
        const message = new MessageCreator().formRaceStatus(users);
        return message;
    }

    getUserFinishedMessage(id, usersMap) {
        const user = getUsersFromRoom(this.roomId, usersMap).find(user => user.id === id);
        const message = new MessageCreator().formUserFinishedMessage(user);
        return message;
    }

    getWinnersListMessage(usersMap) {
        const users = getUsersFromRoom(this.roomId, usersMap);
        const message = new MessageCreator().forWinnersMessage(users);
        return message;
    }
}

export default Bot;